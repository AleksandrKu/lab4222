<?php
require_once(__DIR__ . '/Autoload.php');
spl_autoload_register(["Autoload", 'load_class']);
$config = require_once __DIR__.'/config.php';

echo "<br>**** Mysqli without preparing   ******************************************************************<";

$mysqli_driver = new \application\libraries\MysqliDriver($config['host'],
	$config['username'], $config['password'], $config['database']);
$query_res = $mysqli_driver->getConnection()->query("SELECT * FROM employees WHERE age >10");
if ($query_res == false) {
	echo $mysqli_driver->getConnection()->error;
} //выведет какая ошибка в запросе
while ($row = @$query_res->fetch_array(MYSQLI_ASSOC)) {
	echo "<br>";
	foreach ($row as $key => $value) {
		echo "<br>";
		printf($key . " : " . $value);
	}
}

echo "<br><br>**** Mysqli with preparing   ******************************************************************<br>";

$mysqli_driver = new \application\libraries\MysqliDriver($config['host'],
	$config['username'], $config['password'], $config['database']);
$statement = $mysqli_driver->getConnection()->prepare("SELECT * FROM employees WHERE salary > ? and age < ?");
$id1 = 10000;  $id2 = 45;
$statement->bind_param("ii", $id1, $id2);
$statement->execute();
printf("<br>Salary mote than %s  and age less than  %s<br>", $id1, $id2);
$statement->bind_result($res1,$res2,$res3,$res4,$res5,$res6);  // кол переменных должно равно быть колличеству колонок в таблице
while($statement->fetch() ) {
echo("First name: ".$res2." Last name: ".$res3." Position: ".$res4." Age:".$res5." Salary: ".$res6."<br>");
}

echo "<br>**** PDO without preparing   ********************************************************************<br>";

$pdo_driver = new \application\libraries\PDODriver($config['host'],
	$config['username'], $config['password'], $config['database']);
$query_res = $pdo_driver->getConnection()->query("SELECT * FROM employees");
var_dump($pdo_driver);
while($row=$query_res->fetch(PDO::FETCH_ASSOC))
{
	echo $row['firstname']." ". $row['lastname']." ( {$row['title']} ) <br>";
}

echo "<br>*******    PDO with preparing   ******************************************************************<br>";

$pdo_driver = new \application\libraries\PDODriver($config['host'],
	$config['username'], $config['password'], $config['database']);
$age = 25;
$statement = $pdo_driver->getConnection()->prepare("SELECT * FROM employees WHERE age > ?");
 $statement->execute([$age]);
foreach ($statement as  $row)
{
	echo  $row['firstname'] . " ". $row['lastname'] ." ".$row['age'] ."years,  Salary: ".$row['salary']."$<br> ";
}

