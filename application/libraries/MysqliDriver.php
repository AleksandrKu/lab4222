<?php
namespace  application\libraries;
class MysqliDriver implements IDriver
{
	private $connection;
	public function __construct($server, $username, $password, $database)
	{
		$this->connection = new \mysqli($server, $username, $password, $database);
	}

	public function getConnection()
	{
		return $this->connection;
	}

	public function __destruct()
	{
		$this->connection->close();
	}
}